#!/bin/bash

cd "${HOME}"

ln -s "${HOME}/.config/bashrc/.bashrc" ".bashrc"
if [[ ! -f "${HOME}/.config/bashrc/.bash_profile" ]]; then
	echo "[[ -f ~/.bashrc ]] && . ~/.bashrc" > "${HOME}/.bash_profile"
fi
