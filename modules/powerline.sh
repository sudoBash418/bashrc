meta() {
	DEPENDS=(_functions _env_id)
	AFTERS=(ps1 tilix)
	DESCRIPTION="Load powerline"
	ENABLED="False"
}

load() {
	if [[ -n "$BASHRC_POWERLINE_ENABLED" ]] && match_env_case hostname-TB-Computer && [[ -n "$VTE_VERSION" ]] ; then
		POWERLINE_BASH_CONTINUATION=1
		POWERLINE_BASH_SELECT=1

		powerline-daemon -q

		source /usr/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh
	fi
}
