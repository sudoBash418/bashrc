meta() {
	DEPENDS=(
		_functions
		tput_detection
	)
	LOCALS=(
		__chc
	)
}

load() {
	BASHRC__ps1_update() {
		__updated_time="$(date '+%F %T')"
	}

	__chc() {
		# short for tput color
		tput setaf "$1"
	}

	if [[ -n "$PROMPT_COMMAND" ]]; then
		PROMPT_COMMAND="${PROMPT_COMMAND}; BASHRC__ps1_update"
	else
		PROMPT_COMMAND="BASHRC__ps1_update"
	fi

	declare -A BASHRC_PS1_TABLES

	BASHRC_PS1_TABLES=(
		['PS1_8']="1 7 2 6 4 3"
		['PS1_16']="1 7 2 6 4 3"
		['PS1_256']="196 15 46 51 26 226"
	)

	BASHRC_PS1_NOCOLOR() {
		echo "\h:\u \w \\$"
	}
	BASHRC_PS1_COLOR() {
		echo "\[$(tput bold)\]\[$(__chc $1)\]\h\[$(__chc $2)\]:\[$(__chc $3)\]\u \[$(__chc $4)\]\w\[$(tput sgr0)\]\n\[$(__chc $5)\]\$__updated_time \[$(__chc $6)\]\\$ \[$(tput sgr0)\]"
	}

	if [[ -z "$BASHRC_TPUT_UNAVAILABLE" ]]; then
		case "$(tput colors)" in
			256)
				PS1="$(BASHRC_PS1_COLOR ${BASHRC_PS1_TABLES['PS1_256']})"
				;;
			16)
				PS1="$(BASHRC_PS1_COLOR ${BASHRC_PS1_TABLES['PS1_16']})"
				;;
			8)
				PS1="$(BASHRC_PS1_COLOR ${BASHRC_PS1_TABLES['PS1_8']})"
				;;
			*)
				if isInteractive; then
					echo "We can't tell how many colors this terminal supports."
				fi
				PS1="$(BASHRC_PS1_NOCOLOR)"
				;;
		esac
	else
		PS1="$(BASHRC_PS1_NOCOLOR)"
	fi
}
