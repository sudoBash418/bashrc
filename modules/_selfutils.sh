#!/bin/bash

# utils for distributing and updating the bashrc framework

keybase_target="${XDG_RUNTIME_DIR}/keybase/kbfs/public/sudobash"
# update keybase
if [[ -d "${keybase_target}" ]]; then
	tar -c --exclude-vcs{,-ignores} -C "${HOME}/.config" -f "${keybase_target}/bashrc.tar" "bashrc"
fi
