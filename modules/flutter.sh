meta() {
    DEPENDS=(
    	_env_id
    	path
    )
    DESCRIPTION="Add flutter to PATH on TB-Computer"
}

load() {
    local flutter_path_tmp

    flutter_path_tmp="$HOME/Desktop/programming/langs/flutter"

    if match_env_case hostname-TB-Computer && [[ -d "$flutter_path_tmp" ]]; then
        add_to_path -s "${flutter_path_tmp}/bin"
    fi
}
