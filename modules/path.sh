meta() {
	DEPENDS=(
		_env_id
		_sensible_code
	)
	DESCRIPTION="Modify the PATH"
}

load() {
	add_to_path() {
		local end

		case "$1" in
			-e|--end)
				end=1
				shift
				;;
			-s|--start)
				end=0
				shift
				;;
			*)
				end=0
				;;
		esac

		for thing in "$@"; do
			if ! [[ "${PATH}" == *"${thing}"* ]] && is_dir "${thing}"; then
				if ((${end})); then
					PATH="${PATH}:${thing}"
				else
					PATH="${thing}:${PATH}"
				fi
			fi
		done
	}


	add_to_path -s "${HOME}/bin"
	add_to_path -s "${HOME}/.local/bin"

	# for dart
	add_to_path -e "${HOME}/.pub-cache/bin"


	# Fix Debian PATH
	if match_env_case distro-debian; then
		add_to_path -e "/usr/sbin" "/sbin"
	fi
}
