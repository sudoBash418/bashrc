meta() {
	DESCRIPTION="Environment detection for other modules to use"
	DEPENDS=(_functions)
	LOCALS=(
		add_to_env_case
	)
}

load() {
	# note: don't `declare -x BASHRC_ENV_CASE` - it causes crazy issues

	# function to add case(es) to the array
	add_to_env_case() {
		for case in "$@"; do
			BASHRC_ENV_CASE[${#BASHRC_ENV_CASE[@]}]="$case"
		done
	}

	# function to check if BASHRC_ENV_CASE matches all arguments passed
	match_env_case() {
		for case in "$@"; do
			for x in "${BASHRC_ENV_CASE[@]}"; do
				if [[ "${case}" == "${x}" ]]; then
					return 0
				fi
			done
		done
		return 1
	}


	# logics

	# check if running over adb
	if [[ -n "$ANDROID_SOCKET_adbd" ]]; then
		add_to_env_case adb
	fi

	# check if running over ssh
	if [[ -n "$SSH_CONNECTION" ]]; then
		add_to_env_case ssh
	fi

	# check architechure
	if [[ "$MACHTYPE" =~ "x86_64" ]]; then
		add_to_env_case amd64
	elif [[ "$MACHTYPE" =~ "aarch64" ]] || [[ "$MACHTYPE" =~ "arm" ]]; then
		add_to_env_case arm
	fi

	# check platform
	add_to_env_case "os-${OSTYPE}"

	# linux distro checking
	if [[ -r "/etc/os-release" ]]; then
		add_to_env_case "distro-$(
			. /etc/os-release
			echo $ID
		)"
	fi

	# check hostname
	add_to_env_case "hostname-${HOSTNAME}"
	: 'case "$HOSTNAME" in
		"TB-Computer")
			add_to_env_case TB-Computer
			;;
		"TB-Tablet")
			add_to_env_case TB-Tablet
			;;
		"ArchUSB")
			add_to_env_case ArchUSB
			;;
		*)
			add_to_env_case "$HOSTNAME"
			;;
	esac'

	# check user
	add_to_env_case "user-${USER}"

	# function for other things to check if an Arch package is installed
	arch_pkg_installed() {
		# actually pretty fast (~0.03 seconds)
		package="$1"
		if match_env_case distro-arch; then
			silent pacman -Qq "$package"
			return $?
		else
			return 1
		fi
	}

	# desktop definition
	# (something you sit down in front of and use a GUI with)
	_local_desktops="hostname-"{TB-Computer,ArchUSB,Bork-PC}
	for thing in $_local_desktops; do
		if match_env_case "${thing}"; then
			add_to_env_case desktop
			break
		fi
	done
}
