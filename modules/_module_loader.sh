# module loader + dependency "handler"

# TODO:
# Implement MATCH_ENV?
# Module loader would check all items in array to see if 
# TODO:
# We need a way to say "and" as well as "or", so in our metadata arrays
# we can say ((this1 and that2, or that3) and this4) or that5

# NOTES:
# - if you have a local variable and call another function from that function, that
#   function will still see the local variable
# - functions cannot be local; therefore, if you're in a function that defines func(),
#   and call func2() which defines func() as something else, when you return from
#   func2(), func() will have func2() *supposed to be* local function


mod_fail() {
	BASHRC_FAILED_MODULES[${#BASHRC_FAILED_MODULES[@]}]="${module_name}"
}

debug_print() {
	if [[ -n "$DEBUG" ]]; then
		if [[ "$1" == "show_mod" ]]; then
			shift
			echo "BASHRC: module \"${module_name}\":" "$@"
		else
			echo "BASHRC: " "$@"
		fi
	fi
}

bool_parse() {
	# boolean parser
	# returns an error code of 0 for true, 1 for false
	# if it's not a valid boolean, return the 2nd param
	# if no 2nd param passed, return 1 (for false)
	local input default
	input="$1"
	
	# if no default, the default's default is false
	if [[ -n "$2" ]]; then
		default="$2"
	else
		default=1
	fi

	case "$input" in
		[yY]es|[tT]rue|[oO]n|1)
			return 0
			;;
		[nN]o|[fF]alse|[oO]ff|0)
			return 1
			;;
		*)
			# default
			return $default
			;;
	esac
}

load_module() {
	# return values:
	# 0 - everything loaded fine
	# 1 - failed while loading module - shouldn't try to load again - possibily in a broken state
	# 2 - sanity checks have failed, etc - module didn't load - error
	# 3 - module already loaded, etc - module didn't load - success

	local module_{file,name,afters,deps,locals}
	unset -f load meta
	local AFTERS DEPENDS DESCRIPTION ENABLED # metadata fields

	module_file="$1"

	: "${module_file##*/}"
	module_name="${_%.*}"

	# sanity
	if ! [[ -r "${module_file}" ]]; then
		return 2
	elif is_module_loaded "${module_name}"; then
		return 3
	elif [[ "${module_file}" == "_"* ]]; then
		# don't load meta functions, those should be handled differently
		return 3
	fi

	# load the module file
	source "${module_file}" || {
		mod_fail
		debug_print "sourcing \"${module_file}\" has failed"
		return 1
	}

	# sanity checks on the content of the module
	[[ "$(type -t meta)" == "function" ]] || {
		mod_fail
		debug_print show_mod "missing meta function"
		return 1
	}
	[[ "$(type -t load)" == "function" ]] || {
		mod_fail
		debug_print show_mod "missing load function"
		return 1
	}

	# load metadata
	meta || {
		mod_fail
		debug_print show_mod "failed loading metadata"
		return 1
	}

	# parse enabled status
	if ! bool_parse "$ENABLED" 0; then
		return 2
	fi

	# parse interactive status
	bool_parse "$INTERACTIVE" 2
	case "$?" in
		0)
			# should be interactive
			isInteractive || {
				debug_print show_mod "not loading because shell isn't interactive"
				return 2
			}
			;;
		1)
			# should not be interactive
			isInteractive || {
				debug_print show_mod "not loading because shell is interactive"
				return 2
			}
			;;
		2)
			# doesn't matter
			true
			;;
	esac

	# load afters (*not* resistant to anything complicated)
	for after in "${AFTERS[@]}"; do
		# only load if it's not already loaded
		if ! is_module_loaded "${after}"; then
			load_module "${BASHRC_MODULE_DIRECTORY}/${after}.sh"
			case "$?" in
				1)
					# if it failed to load, exit with error code
					mod_fail
					debug_print show_mod "failed loading dependency"
					return 1
					;;
				0|2|3)
					# if loading dependency worked (or failed for a good reason), continue
					continue
					;;
			esac
		fi
	done

	# load dependencies (*not* resistant to anything complicated)
	for dep in "${DEPENDS[@]}"; do
		# only load if it's not already loaded
		if ! is_module_loaded "${dep}"; then
			load_module "${BASHRC_MODULE_DIRECTORY}/${dep}.sh"
			case "$?" in
				1|2)
					# if it failed to load, exit with error code
					return 1
					;;
				0|3)
					# if loading dependency worked, continue
					continue
					;;
			esac
		fi
	done

	# reload the module to ensure we still have the correct load()
	source "${module_file}"

	# actually load the module
	load

	# unset all local functions from the module
	for local_func in "${LOCALS[@]}"; do
		unset -f "$local_func"
	done

	# mark this module as loaded
	BASHRC_LOADED_MODULES[${#BASHRC_LOADED_MODULES[@]}]="${module_name}"

	# return
	return 0
}

# ensure BASHRC_MODULE_DIRECTORY exists
if ! [[ -d "${BASHRC_MODULE_DIRECTORY}" ]]; then
	echo "ERROR: \$BASHRC_MODULE_DIRECTORY (${BASHRC_MODULE_DIRECTORY}) not found"
	return 1
fi

# find module files and load them
for module_file in "${BASHRC_MODULE_DIRECTORY}/"*.sh; do
	# Skip modules that start with _
	if ! [[ "${module_file##*/}" == _* ]]; then
		load_module "${module_file}"
	fi
done

# unset module loader functions
unset -f debug_print bool_parse
