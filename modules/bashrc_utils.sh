meta() {
	ENABLED=0
	DESCRIPTION=""
	DEPENDS=()
	AFTERS=()
	INTERACTIVE="both"
}

load() {
	bashrc-update() {
		pushd ${BASHRC_MODULE_DIRECTORY}
		if binary_exists git; then
			git pull
		elif binary_exists curl && binary_exists tar; then
			curl "https://gitlab.com/sudoBash418/bashrc/repository/master/archive.tar" -o bashrc.tar
			tar xf
		fi
	}
}
