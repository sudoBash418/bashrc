meta() {
	DESCRIPTION="Set tab length for the terminal"
	DEPENDS=(_functions)
}

load() {
	if binary_exists tabs; then
		tabs -4
	fi
}
