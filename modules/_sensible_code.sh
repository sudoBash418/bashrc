meta() {
	DESCRIPTION="A pile of functions for writing more concise and readable bash"
}

load() {
	silent() {
		"$@" >/dev/null 2>&1
	}

	binary_exists() {
		silent type "$1"
	}

	is_dir() {
		[[ -d "$1" ]]
	}
}
