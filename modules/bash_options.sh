meta() {
	ENABLED=1
	DESCRIPTION="Set bash options for the interactive shell"
	DEPENDS=(_functions _env_id)
	INTERACTIVE=1
}

load() {
	if match_env_case desktop; then
		shopt -s cdspell		# fix spelling
		shopt -s checkwinsize	# update $ROWS and $COLUMNS
		shopt -s dotglob		# match files starting with . with *
	fi
}
