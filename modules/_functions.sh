# base functions
# completely written in plain bash 4 and should be portable

# initalize loaded modules list
#declare -ax BASHRC_LOADED_MODULES
BASHRC_LOADED_MODULES[0]="_functions"
declare -a BASHRC_FAILED_MODULES

# bashrc.d functions

quiet() {
	"$@" >/dev/null
}

silent() {
	"$@" >/dev/null 2>&1
}

binary_exists() {
	silent type "$1"
}

isInteractive() {
	[[ $- == *i* ]]
}

is_module_loaded() {
	module_to_check="$1"

	for module in "${BASHRC_LOADED_MODULES[@]}"; do
		if [[ "${module}" == "${module_to_check}" ]]; then
			return 0
		fi
	done

	return 1
}

is_module_failed() {
	module_to_check="$1"

	for module in "${BASHRC_FAILED_MODULES[@]}"; do
		if [[ "${module}" == "${module_to_check}" ]]; then
			return 0
		fi
	done

	return 1
}

sudoMode() {
	binary_exists sudo && silent sudo -n true
}
