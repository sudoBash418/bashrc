meta() {
	DESCRIPTION="Set up environment variables"
	DEPENDS="_functions"
}

load() {
	local _tmp_SSH_AUTH_SOCK
	
	export_if_dir() {
		# pass this variable *NAMES*, like 'HOME' (or '$HOME', doesn't matter)
	
		# iterate through 'em all
		for test_this in "$@"; do
			# in case we got a $variable, remove the $
			if [[ "$test_this" == '$'* ]]; then
				test_this="${test_this:1}"
			fi
	
			# if it exists, export it, otherwise unset it
			if [[ -d "${!test_this}" ]]; then
				export "$test_this"
			else
				unset "$test_this"
			fi
		done
	}


	# Set up android dev variables

	ANDROID_HOME="$HOME/android-sdk"
	ANDROID_NDK_HOME="/opt/android-ndk"
	
	export_if_dir ANDROID_HOME ANDROID_NDK_HOME


	# GPG env fix

	export GPG_DIR="$(tty)"


	# Fix SSH agent things

	_tmp_SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh"

	if [[ -S "${_tmp_SSH_AUTH_SOCK}" ]]; then
		unset SSH_AGENT_PID
		export SSH_AUTH_SOCK="${_tmp_SSH_AUTH_SOCK}"
	fi


	# Load dircolors if available

	if [[ -r "$HOME/.dircolors" ]]; then
		if binary_exists dircolors; then
			eval $(dircolors -b $HOME/.dircolors)
		fi
	fi
}
