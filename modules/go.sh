meta() {
	DESCRIPTION="Set up Go environment"
	DEPENDS=(
		_env_id
		_sensible_code
		path
	)
}

load() {
	local tmp_go_path="${HOME}/Desktop/programming/go"
	if match_env_case hostname-TB-Computer; then
		if is_dir "$tmp_go_path"; then
			export GOPATH="$tmp_go_path"
			add_to_path "${tmp_go_path}/bin"
		fi
	fi
}
