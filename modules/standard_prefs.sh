meta() {
	ENABLED=1
	DESCRIPTION=""
	DEPENDS=(_functions)
	AFTERS=()
}

load() {
	# set editor
	if silent type "micro"; then
		EDITOR="micro"
	elif silent type "nano"; then
		EDITOR="nano"
	fi

	# set pager
	if silent type "less"; then
		PAGER="less"
	elif silent type "more"; then
		PAGER="more"
	fi

	# export prefs so other programs started in bash can use them
	export EDITOR PAGER
}
