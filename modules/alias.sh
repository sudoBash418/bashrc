meta() {
	DESCRIPTION="Aliases for bash"
	DEPENDS=(_functions _env_id)
	AFTERS=(standard_prefs)
}

load() {
	# color aliases
	alias ls="ls --color=auto"

	# cd
	cd() {
		if [[ -z "$1" ]]; then
			quiet pushd "$HOME"
		else
			quiet pushd "$1"
		fi
	}
	alias cdd='quiet popd'


	# other aliases

	if binary_exists tr; then
		alias upper="tr \'[:lower:]\' \'[:upper:]\'"
		alias lower="tr \'[:upper:]\' \'[:lower:]\'"
	fi
	if binary_exists du; then
		alias foldersize="du -s"
	fi
	if (( UID != 0 )); then
		alias reboot="sudo systemctl reboot"
		alias poweroff="sudo systemctl poweroff"
	fi
	if ! match_env_case android; then
		# if not on android, set the grep color alias
		# android grep doesn't support --color
		alias grep="grep --color=auto"
	fi
	if match_env_case TB-Computer; then
		alias moshtablet="mosh --no-ssh-pty tb-tablet"
	fi
	if [[ "$PAGER" == "less" ]] && binary_exists man; then
		man() {
			LESS_TERMCAP_md=$'\e[01;31m' \
			LESS_TERMCAP_me=$'\e[0m' \
			LESS_TERMCAP_se=$'\e[0m' \
			LESS_TERMCAP_so=$'\e[01;44;33m' \
			LESS_TERMCAP_ue=$'\e[0m' \
			LESS_TERMCAP_us=$'\e[01;32m' \
			command man "$@"
		}
	fi

	if [[ -e "$HOME/node_modules/.bin/igdm" ]]; then
		alias insta_sudobash418="$HOME/node_modules/.bin/igdm -u sudobash418 -p \$(gkeyring -k Instagram --name sudobash418|cut -f3)"
	fi

	if binary_exists lsblk; then
		alias lsblk-long="lsblk -o NAME,SIZE,TYPE,FSTYPE,RO,HOTPLUG,MOUNTPOINT,LABEL,PARTLABEL,UUID,PARTUUID"
	fi

	if binary_exists exa; then
		alias rls="$(which ls)"
		alias ls="exa -g"
	fi
}
