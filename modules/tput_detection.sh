meta() {
	DEPENDS=(_functions)
}

load() {
	if ! silent type tput; then
		BASHRC_TPUT_UNAVAILABLE=true
	fi
}
