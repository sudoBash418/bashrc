meta() {
	DEPENDS=(_functions)
}

load() {
	# tilix fix - stolen from /etc/profile.d/vte.sh
	# note: if editing PROMPT_COMMAND in another place, it should depend on this because this fix will overwrite it

	vte_fix() {
		__vte_urlencode() (
			# This is important to make sure string manipulation is handled
			# byte-by-byte.
			LC_ALL=C
			str="$1"
			while [ -n "$str" ]; do
				safe="${str%%[!a-zA-Z0-9/:_\.\-\!\'\(\)~]*}"
				printf "%s" "$safe"
				str="${str#"$safe"}"
				if [ -n "$str" ]; then
					printf "%%%02X" "'$str"
					str="${str#?}"
				fi
			done
		)

		__vte_osc7 () {
			last_command=$(history | tail -n1 - | cut -c8-)
			printf "\033]777;notify;Command completed;%s\007\033]7;file://%s%s\007" "${last_command}" "${HOSTNAME:-}" "$(__vte_urlencode "${PWD}")"
		}

		__vte_prompt_command() {
			local command=$(HISTTIMEFORMAT= history 1 | sed 's/^ *[0-9]\+ *//')
			command="${command//;/ }"
			local pwd='~'
			[ "$PWD" != "$HOME" ] && pwd=${PWD/#$HOME\//\~\/}
			printf "\033]777;notify;Command completed;%s\007\033]0;%s@%s:%s\007%s" "${command}" "${USER}" "${HOSTNAME%%.*}" "${pwd}" "$(__vte_osc7)"
		}

		case "$TERM" in
			xterm*|vte*)
				PROMPT_COMMAND="${PROMPT_COMMAND}; __vte_prompt_command"
				;;
		esac
	}

	if [[ $TILIX_ID ]] || [[ $VTE_VERSION ]]; then
		vte_fix
	fi
}
