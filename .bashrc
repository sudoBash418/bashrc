# bashrc

# fix $HOME
if [[ -z "$HOME" ]]; then export HOME=~; fi

# powerline config (comment to disable)
#BASHRC_POWERLINE_ENABLED=1

# define folder for modules
BASHRC_MODULE_DIRECTORY="${HOME}/.config/bashrc/modules"

# load basic bash functions
source "${BASHRC_MODULE_DIRECTORY}/_functions.sh"

# load module loader
source "${BASHRC_MODULE_DIRECTORY}/_module_loader.sh"

# load liquidprompt
#source "$HOME/liquidprompt/liquidprompt"

# warn about failed modules if interactive
if isInteractive && [[ -n "$BASHRC_FAILED_MODULES" ]] ; then
	echo "WARNING: these bashrc modules have failed to load:"
	echo "${BASHRC_FAILED_MODULES[@]}"
fi

return 0
